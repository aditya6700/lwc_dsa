import { LightningElement } from 'lwc';

export default class InterestCalculator extends LightningElement {
    siInputs = {
        principal: 0,
        time: 0,
        rate: 0,
    };
    simpleInterest;

    siInputHandler(event) {
        const { value, name } = event.target;
        this.siInputs = {
            ...this.siInputs,
            [name]: value,
        }
    }

    calculateInterest(event) {
        event.preventDefault();
        const result = +this.siInputs.principal * +this.siInputs.rate * +this.siInputs.time / 100;
        this.simpleInterest = `Simple Interest is ${result}`;
    }

}