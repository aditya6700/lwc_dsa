import { LightningElement } from 'lwc';

export default class LifeCycleParent extends LightningElement {
    constructor() {
        super();
        console.log("PARENT constructor called");
    }

    connectedCallback() {
        console.log("PARENT connectedCallback called");
    }

    renderedCallback() {
        console.log("PARENT renderedCallback called");
    }

    disconnectedCallback() {
        console.log("PARENT disconnectedCallback called");
    }
}