import { LightningElement } from 'lwc';

export default class HelloLWC extends LightningElement {

    // input form handler
    greeting = {
        name: '',
        age: '',
        phone: ''
    };

    changeHandler(event) {
        const { value, name } = event.target;
        this.greeting = {
            ...this.greeting,
            [name]: value,
        }
    };

    // conditional rendering
    number = 1;
    even = false;

    numberHandler(event) {
        this.number = event.target.value;
        this.even = (this.number % 2 === 0) ? true : false;
    }

    // take input on click and change the input using conditional rendering
    input = false;

    handleInput(event) {
        event.preventDefault();
        this.input = true;
    }

    handleChange(event) {
        event.preventDefault();
        this.input = false;
    }

    handleClear(event) {
        event.preventDefault();
        this.greeting = {};
    }

    // for each directive
    opportunities = [
        {
            name: 'opportunity 1',
            status: 'closed',
            date: new Date("2020-12-05").toLocaleDateString(),
        },
        {
            name: 'opportunity 2',
            status: 'open',
            date: new Date("2021-12-25").toLocaleDateString(),
        },
        {
            name: 'opportunity 3',
            status: 'working',
            date: new Date("2020-2-10").toLocaleDateString(),
        },
        {
            name: 'opportunity 4',
            status: 'paused',
            date: new Date("2021-11-15").toLocaleDateString(),
        },
    ]

}