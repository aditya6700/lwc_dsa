import { api, LightningElement } from 'lwc';

export default class BaseRecordForm extends LightningElement {
    @api recordId;
}