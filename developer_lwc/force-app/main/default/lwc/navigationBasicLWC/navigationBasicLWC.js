import { api, LightningElement } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

export default class NavigationBasicLWC extends NavigationMixin(LightningElement) {
    @api recordId;

    navigateToNewRecordPage() {
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                actionName: "new",
                recordId: this.recordId,
                objectApiName: "Account"
            }
        });
    }

    navigateToEditRecordPage() {
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                actionName: "edit",
                recordId: this.recordId,
                objectApiName: "Account"
            }
        });
    }

    navigateToViewRecordPage() {
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                actionName: "view",
                recordId: this.recordId,
                objectApiName: "Account"
            }
        });
    }

    navigateToRecentListView() {
        this[NavigationMixin.Navigate]({
            type: 'standard__objectPage',
            attributes: {
                actionName: "list",
                objectApiName: "Account"
            },
            sate: {
                filterList: 'Recent'
            }
        });
    }

    navigateToRelatedListView() {
        this[NavigationMixin.Navigate]({
            type: 'standard__recordRelationshipPage',
            attributes: {
                actionName: "view",
                recordId: this.recordId,
                objectApiName: "Account",
                relationshipApiName: "Contacts"
            }
        });
    }

    navigateToOpp() {
        this[NavigationMixin.Navigate]({
            type: 'standard__objectPage',
            attributes: {
                actionName: "home",
                objectApiName: "Opportunity"
            }
        });
    }

    navigateToLeads() {
        this[NavigationMixin.Navigate]({
            type: 'standard__objectPage',
            attributes: {
                actionName: "home",
                objectApiName: "Lead"
            }
        });
    }

    navigateToTrailHead() {
        this[NavigationMixin.Navigate]({
            type: 'standard__webPage',
            attributes: {
               url: "https://trailhead.salesforce.com/"
            }
        });
    }

    navigateToHomePage() {
        this[NavigationMixin.Navigate]({
            type: 'standard__namedPage',
            attributes: {
                pageName: "home"
            }
        });
    }

    navigateToChatterPage() {
        this[NavigationMixin.Navigate]({
            type: 'standard__namedPage',
            attributes: {
                pageName: "chatter"
            }
        });
    }

}