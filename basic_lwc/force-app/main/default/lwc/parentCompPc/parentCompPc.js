import { LightningElement } from 'lwc';

export default class ParentCompPc extends LightningElement {
    handleChange(event) {
        this.template.querySelector('c-child-comp-pc').changeCase(event.target.value);
    }
}