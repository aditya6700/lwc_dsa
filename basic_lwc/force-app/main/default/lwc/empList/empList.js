import { LightningElement } from 'lwc';

export default class EmpList extends LightningElement {
    empDetails = [
        {
            name: "joe",
            address: "SA"
        },
        {
            name: "Paul",
            address: "UK"
        },
        {
            name: "Tej",
            address: "CA"
        },
        {
            name: "Sarah",
            address: "LA"
        },
    ]
}