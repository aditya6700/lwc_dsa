import { LightningElement } from 'lwc';

export default class ParentCompCp extends LightningElement {
    htmlMsg;
    jsMsg;

    // via addEventListener
    constructor() {
        super();
        this.template.addEventListener('mycustomevent', this.myEventHandlerJs.bind(this));
    }

    myEventHandler(event) {
        this.htmlMsg = event.detail;
    }
    myEventHandlerJs(event) {
        this.jsMsg = event.detail;
    }
}