/**
 * @description Update Name in Object using LWC
 */
public with sharing class UpdateFlowURLObj {


    @AuraEnabled
    public static void updateName(String name, Id recordId){
        try {
            Flow_URL__c rec = [SELECT Name FROM Flow_URL__c WHERE Id= :recordId];
            rec.Name = name;
            update rec; 
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
  
}
