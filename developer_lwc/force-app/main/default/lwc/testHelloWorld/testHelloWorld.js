import { api, LightningElement } from 'lwc';

export default class TestHelloWorld extends LightningElement {
    @api greeting = 'World';
}