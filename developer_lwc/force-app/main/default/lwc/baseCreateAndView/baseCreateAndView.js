import { LightningElement } from 'lwc';

export default class BaseCreateAndView extends LightningElement {
    recordId;
    create = true;

    createContact(event) {
        this.recordId = event.detail.id;
        this.create = false;
    }

    handleClick() {
        this.create = true;
    }
}