/**
 * @author a b
 * @description Update Name in Object using LWC
 */
public with sharing class UpdateNameTest {


    @AuraEnabled
    public static void updateName(String name, String recordId){
        try {
            Flow_URL__c rec = [SELECT Name, Digit__c FROM Flow_URL__c WHERE Id= :recordId];
            // rec.Name = String.valueOf(rec.Digit__c) ;
            rec.Name = name;
            update rec; 
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
  
}
