import { LightningElement, wire } from 'lwc';
import { registerListener, unregisterAllListeners } from 'c/pubsub';
import { CurrentPageReference } from 'lightning/navigation';

export default class CanvaSubscriber extends LightningElement {
    color;
    @wire(CurrentPageReference) pageRef;

    connectedCallback() {
        registerListener('changedColor', this.handleColorChange, this);
    }

    disconnectedCallback() {
        unregisterAllListeners(this);
        console.log('disconnected');
    }

    handleColorChange(colorCode) {
    console.log("Color change -->"+colorCode);
       this.color=colorCode;
    }

    get colorStyle() {
        console.log('in colorstyle')
        return  `background-color:${this.color}`;
    }

}