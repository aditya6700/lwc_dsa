import { LightningElement } from 'lwc';

export default class ChildCompCp extends LightningElement {
    handleChange(event) {
        let name = event.target.value;
        const myEvent = new CustomEvent('mycustomevent', {
            detail: name,
            bubbles: true
        });
        this.dispatchEvent(myEvent);
        // this.dispatchEvent(new CustomEvent('mycustomevent',{msg:name,bubbles:true}));
    }
}