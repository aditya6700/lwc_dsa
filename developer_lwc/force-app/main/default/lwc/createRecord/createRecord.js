import { LightningElement } from 'lwc';
import { createRecord } from 'lightning/uiRecordApi';

export default class CreateRecord extends LightningElement {
    form = {
        name: '',
        phone: '',
        email:'',
    }
    
    inputChangeHandler(event) {
        const { value, name } = event.target;
        this.form = {
            ...this.form,
            [name]:value
        } 
    }

    createRecord() {
        const fields = {
            'Tag__c': this.form.name,
            'Phone__c': this.form.phone,
            'Email__c': this.form.email
        };
        const recordObj = { apiName: 'Test__c', fields };
        createRecord(recordObj)
            .then((response) => {
                console.log(`Test record has been successfully created with id: ${response.id}`+JSON.stringify(response));
                alert('Record created');
                this.form = {};
            })
            .catch((error) => {
                console.log(`An error encountered while creating record: ${JSON.stringify(error)}`);
            })
    }
}