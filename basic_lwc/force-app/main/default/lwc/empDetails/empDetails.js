import { api, LightningElement } from 'lwc';

export default class EmpDetails extends LightningElement {
    @api empDetail = {
        name: 'John',
        address: 'DC'
    }
}