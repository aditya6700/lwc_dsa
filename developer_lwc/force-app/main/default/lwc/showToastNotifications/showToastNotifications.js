import { LightningElement } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class ShowToastNotifications extends LightningElement {
    
    handleSuccess() {
        this.dispatchEvent(new ShowToastEvent({
            title: 'Success',
            message: 'This is a Success Message',
            variant: 'success'
        }));
    }

    handleError() {
        this.dispatchEvent(new ShowToastEvent({
            title: 'Error',
            message: 'This is an Error Message',
            variant: 'error'
        }));
    }
    
    handleWarning() {
        this.dispatchEvent(new ShowToastEvent({
            title: 'Warning',
            message: 'This is a Warning Message',
            variant: 'warning'
        }));    
    }
    
    handleInfo() {
        this.dispatchEvent(new ShowToastEvent({
            title: 'Info',
            message: 'This is an Information',
            variant: 'info'
        }));
    }
    
}