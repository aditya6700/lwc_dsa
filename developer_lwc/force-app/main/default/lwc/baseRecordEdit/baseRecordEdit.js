import { api, LightningElement } from 'lwc';

export default class BaseRecordEdit extends LightningElement {
    @api recordId;
}