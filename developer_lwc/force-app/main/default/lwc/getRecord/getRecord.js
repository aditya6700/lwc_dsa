import { LightningElement, wire } from 'lwc';
import { getRecord, createRecord } from 'lightning/uiRecordApi';

const fieldArray = ['Test__c.Tag__c', 'Tag__c.Phone__c', 'Tag__c.Email__c'];

export default class GetRecord extends LightningElement {
    form = {
        name: '',
        phone: '',
        email: '',
    };
    recordId;
    created = false;

    @wire(getRecord, { recordId: '$recordId', fields: fieldArray }) testRecord;

    inputChangeHandler(event) {
        const { value, name } = event.target;
        this.form = {
            ...this.form,
            [name]: value,
        }
    }

    newRecord() {
        this.created = false;
    }

    createRecord() {
        const fields = {
            'Tag__c': this.form.name,
            'Phone__c': this.form.phone,
            'Email__c': this.form.email
        };
        const recordObj = { apiName: 'Test__c', fields };
        createRecord(recordObj)
            .then((response) => {
                console.log(`Test record has been successfully created with id: ${response.id}`+JSON.stringify(response));
                //alert('Record created');
                this.form = {};
                this.recordId = response.id;
                this.created = true;
            })
            .catch((error) => {
                console.log(`An error encountered while creating record: ${JSON.stringify(error)}`);
            })
    }


    get getName() {
        return this.testRecord.data ? this.testRecord.data.fields.Tag__c.value : undefined;
    }
    get getPhone() {
        return this.testRecord.data ? this.testRecord.data.fields.Phone__c.value : undefined;
    }
    get getEmail() {
        return this.testRecord.data ? this.testRecord.data.fields.Email__c.value : undefined;
    }


}