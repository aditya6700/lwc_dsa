import { api, LightningElement } from 'lwc';

export default class ChildCompPc extends LightningElement {
    msg;
    @api
    changeCase(msg) {
        this.msg = msg.toUpperCase();
    }
}