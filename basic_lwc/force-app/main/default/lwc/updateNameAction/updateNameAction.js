import { LightningElement, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { CloseActionScreenEvent } from 'lightning/actions';
import updateName from '@salesforce/apex/UpdateNameTest.updateName';


export default class UpdateNameAction extends LightningElement {

   @api recordId;
    newName;
    isSpinner = false;

    changeEvent(event) {
        this.newName = event.target.value;
    }

    closeAction(event) {
        this.dispatchEvent(new CloseActionScreenEvent());
    }

    saveAction(event) {
        this.isSpinner = true;
        event.preventDefault();
        updateName({
            name: this.newName,
            recordId: this.recordId
        })
            .then(result => {
            this.dispatchEvent(new ShowToastEvent({
                title: 'Success',
                message: 'Name update Successfully',
                variant: 'success'
            }));
            console.log('Result', result);
            })
            .catch(error => {
                this.dispatchEvent(new ShowToastEvent({
                    title: 'Error',
                    message: error,
                    variant: 'error'
                }));
                console.log('Error:', error);
            }).finally(e => {
                this.isSpinner = false;
                this.closeAction();
                console.log('Finally:');
            });
   }

}