import { createElement } from 'lwc';
import testHelloWorld from 'c/testHelloWorld';

describe('c-test-hello-world', () => {
    afterEach(() => {
        while (document.body.firstChild) {
            document.body.removeChild(document.body.firstChild);
        }
    });

    it('display greeting', () => {
        const element = createElement('c-test-hello-world', {
            is:testHelloWorld
        });
        document.body.appendChild(element);

        const pTag = element.shadowRoot.querySelector('p');
        expect(pTag.textContent).toBe('Hello, World!');
    });

    it('renders with Aditya', () => {
        const element = createElement('c-test-hello-world', {
            is: testHelloWorld
        });
        element.greeting = 'Aditya';
        document.body.appendChild(element);

        const pTag = element.shadowRoot.querySelector('p');
        expect(pTag.textContent).toBe('Hello, Aditya!');
    });
})
