import { LightningElement } from 'lwc';

export default class LifeCycleChild extends LightningElement {
    constructor() {
        super();
        console.log("CHILD constructor called");
    }

    connectedCallback() {
        console.log("CHILD connectedCallback called");
    }

    renderedCallback() {
        console.log("CHILD renderedCallback called");
    }

    disconnectedCallback() {
        console.log("CHILD disconnectedCallback called");
    }
}