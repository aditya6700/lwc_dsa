import { api, LightningElement } from 'lwc';

export default class BaseRecordViewForm extends LightningElement {
    @api recordId;
}